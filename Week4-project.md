# Predicting Personal Activity
Pallavi Gadgil  
September 14, 2017  



## Introduction

Following report aims at analyzing data collected by accelerometers on the belt, forearm, arm, and dumbell of 6 participants. The 6 participants performed activities in the correct and incorrect ways, referenced by the Classe column. The goal of this project is to predict the manner in which they did the exercise.



```r
library(caret)
```

```
## Warning: package 'caret' was built under R version 3.4.1
```

```
## Loading required package: lattice
```

```
## Loading required package: ggplot2
```

```r
library(forecast)
```

```
## Warning: package 'forecast' was built under R version 3.4.1
```

```r
library(ggplot2)
library(rattle)
```

```
## Warning: package 'rattle' was built under R version 3.4.1
```

```
## Rattle: A free graphical interface for data mining with R.
## Version 4.1.0 Copyright (c) 2006-2015 Togaware Pty Ltd.
## Type 'rattle()' to shake, rattle, and roll your data.
```

## Getting and Cleaning Data

###Getting Data

The data for this project comes from this source: http://groupware.les.inf.puc-rio.br/har. 

The training data for this project are available here:

https://d396qusza40orc.cloudfront.net/predmachlearn/pml-training.csv

The test data are available here:

https://d396qusza40orc.cloudfront.net/predmachlearn/pml-testing.csv


```r
training = read.csv("pml-training.csv", na.strings=c("NA","#DIV/0!",""))
testing = read.csv("pml-testing.csv", na.strings=c("NA","#DIV/0!",""))

set.seed(345)
inTrain <- training
```

### Cleaning Data
Removing first 7 columns from the dataset since these primarily contain User info and timestamps. Additionally, the section below removes predictors that have few unique values and thus very low variance. Additionally, variables that have a missing values are also eliminated. 


```r
inTrain <- inTrain[,-c(1:7)]
nzvcol <- nearZeroVar(inTrain)
inTrain <- inTrain[,-nzvcol]
inTrain <-inTrain[, colSums(is.na(inTrain)) == 0]
inTrain <- inTrain[complete.cases(inTrain),]
inTrain$classe <- as.factor(inTrain$classe)
```
The new dimension of the training set are 19622, 53

## Model fitting
### Train and Validate
Splitting the training data set into training (70%) and validation (30%)

```r
trainSplit <- createDataPartition(y=inTrain$classe,p=.7,list=FALSE)

trainSet <- inTrain[trainSplit,]
validationSet <- inTrain[-trainSplit,]
```
### Model 1 - Decision Trees
Building first model using Decision Trees


```r
modFit <- train(classe ~.,method="rpart",data=trainSet)
```

```
## Loading required package: rpart
```

```r
print(modFit$finalModel)
```

```
## n= 13737 
## 
## node), split, n, loss, yval, (yprob)
##       * denotes terminal node
## 
##  1) root 13737 9831 A (0.28 0.19 0.17 0.16 0.18)  
##    2) roll_belt< 130.5 12575 8678 A (0.31 0.21 0.19 0.18 0.11)  
##      4) pitch_forearm< -33.95 1101    9 A (0.99 0.0082 0 0 0) *
##      5) pitch_forearm>=-33.95 11474 8669 A (0.24 0.23 0.21 0.2 0.12)  
##       10) magnet_dumbbell_y< 436.5 9634 6901 A (0.28 0.18 0.24 0.19 0.11)  
##         20) roll_forearm< 124.5 6005 3557 A (0.41 0.18 0.18 0.17 0.062) *
##         21) roll_forearm>=124.5 3629 2417 C (0.079 0.18 0.33 0.23 0.18) *
##       11) magnet_dumbbell_y>=436.5 1840  912 B (0.039 0.5 0.045 0.23 0.18) *
##    3) roll_belt>=130.5 1162    9 E (0.0077 0 0 0 0.99) *
```

```r
plot(modFit$finalModel,uniform = TRUE, main="Classification Tree")
text(modFit$finalModel,use.n=TRUE, all=TRUE, cex=0.8)
```

![](Week4-project_files/figure-html/mod1-1.png)<!-- -->

### Model 2 - Random Forest
Building 2nd model with Random Forest. I am going to limit the number of iterations to 5 so that model can be built with a better performance.


```r
control <- trainControl(method = "cv", number = 5)
modFitRF <-train(classe ~ ., data = trainSet, method = "rf", trControl = control)
```

```
## Loading required package: randomForest
```

```
## Warning: package 'randomForest' was built under R version 3.4.1
```

```
## randomForest 4.6-12
```

```
## Type rfNews() to see new features/changes/bug fixes.
```

```
## 
## Attaching package: 'randomForest'
```

```
## The following object is masked from 'package:ggplot2':
## 
##     margin
```

##Predict
Apply both predcition models on the validation set and gather confusion Matrix

### Prediction and Confusion Matrix for Model 1

```r
pred1 <- predict(modFit,validationSet)
rpartCFM <- confusionMatrix(pred1,validationSet$classe)
rpartCFM
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction    A    B    C    D    E
##          A 1533  493  490  441  151
##          B   23  374   27  161  156
##          C  113  272  509  362  297
##          D    0    0    0    0    0
##          E    5    0    0    0  478
## 
## Overall Statistics
##                                           
##                Accuracy : 0.4918          
##                  95% CI : (0.4789, 0.5046)
##     No Information Rate : 0.2845          
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.335           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: A Class: B Class: C Class: D Class: E
## Sensitivity            0.9158  0.32836  0.49610   0.0000  0.44177
## Specificity            0.6260  0.92267  0.78514   1.0000  0.99896
## Pos Pred Value         0.4932  0.50472  0.32775      NaN  0.98965
## Neg Pred Value         0.9492  0.85128  0.88066   0.8362  0.88819
## Prevalence             0.2845  0.19354  0.17434   0.1638  0.18386
## Detection Rate         0.2605  0.06355  0.08649   0.0000  0.08122
## Detection Prevalence   0.5281  0.12591  0.26389   0.0000  0.08207
## Balanced Accuracy      0.7709  0.62551  0.64062   0.5000  0.72037
```

### Prediction and Confusion Matrix for Model 2

```r
pred2 <- predict(modFitRF,validationSet)
rfCFM<-confusionMatrix(pred2,validationSet$classe)
rfCFM
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction    A    B    C    D    E
##          A 1672    5    0    0    0
##          B    1 1133    8    1    0
##          C    0    1 1016   14    0
##          D    0    0    2  949    4
##          E    1    0    0    0 1078
## 
## Overall Statistics
##                                           
##                Accuracy : 0.9937          
##                  95% CI : (0.9913, 0.9956)
##     No Information Rate : 0.2845          
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.992           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: A Class: B Class: C Class: D Class: E
## Sensitivity            0.9988   0.9947   0.9903   0.9844   0.9963
## Specificity            0.9988   0.9979   0.9969   0.9988   0.9998
## Pos Pred Value         0.9970   0.9913   0.9855   0.9937   0.9991
## Neg Pred Value         0.9995   0.9987   0.9979   0.9970   0.9992
## Prevalence             0.2845   0.1935   0.1743   0.1638   0.1839
## Detection Rate         0.2841   0.1925   0.1726   0.1613   0.1832
## Detection Prevalence   0.2850   0.1942   0.1752   0.1623   0.1833
## Balanced Accuracy      0.9988   0.9963   0.9936   0.9916   0.9980
```
Model with decision tree has accuracy of 0.4917587 and Model with Random forest has accuracy of 0.9937128 . 

## Applying model on Test data set
Since the Random forest model has far better accuracy, this model will be used to predict Classe variable in the Test Data Set.

```r
testingDataSet <- testing[, -c(1:7)]
predict(modFitRF, testingDataSet)
```

```
##  [1] B A B A A E D B A A B C B A E E A B B B
## Levels: A B C D E
```



